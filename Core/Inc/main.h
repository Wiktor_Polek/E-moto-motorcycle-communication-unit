/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define SD_CARD_DETECT_Pin GPIO_PIN_0
#define SD_CARD_DETECT_GPIO_Port GPIOA
#define SD_CARD_SCK_Pin GPIO_PIN_1
#define SD_CARD_SCK_GPIO_Port GPIOA
#define DEBUG_TX_Pin GPIO_PIN_2
#define DEBUG_TX_GPIO_Port GPIOA
#define NRF_CS_Pin GPIO_PIN_3
#define NRF_CS_GPIO_Port GPIOA
#define NRF_CE_Pin GPIO_PIN_4
#define NRF_CE_GPIO_Port GPIOA
#define SD_CARD_SS_Pin GPIO_PIN_5
#define SD_CARD_SS_GPIO_Port GPIOA
#define SD_CARD_MISO_Pin GPIO_PIN_6
#define SD_CARD_MISO_GPIO_Port GPIOA
#define SD_CARD_MOSI_Pin GPIO_PIN_7
#define SD_CARD_MOSI_GPIO_Port GPIOA
#define BtState_Pin GPIO_PIN_0
#define BtState_GPIO_Port GPIOB
#define SD_CARD_INT_Pin GPIO_PIN_1
#define SD_CARD_INT_GPIO_Port GPIOB
#define VEL_MEASURE_Pin GPIO_PIN_8
#define VEL_MEASURE_GPIO_Port GPIOA
#define VEL_MEASURE_EXTI_IRQn EXTI9_5_IRQn
#define IMU_SCL_Pin GPIO_PIN_9
#define IMU_SCL_GPIO_Port GPIOA
#define IMU_SDA_Pin GPIO_PIN_10
#define IMU_SDA_GPIO_Port GPIOA
#define DEBUG_RX_Pin GPIO_PIN_15
#define DEBUG_RX_GPIO_Port GPIOA
#define LED_SCK_Pin GPIO_PIN_3
#define LED_SCK_GPIO_Port GPIOB
#define LED_MOSI_Pin GPIO_PIN_5
#define LED_MOSI_GPIO_Port GPIOB
#define BT_TX_Pin GPIO_PIN_6
#define BT_TX_GPIO_Port GPIOB
#define BT_RX_Pin GPIO_PIN_7
#define BT_RX_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
