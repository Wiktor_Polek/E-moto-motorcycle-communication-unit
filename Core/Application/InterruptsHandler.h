/*
 * InterruptsHandler.h
 *
 *  Created on: Nov 24, 2020
 *      Author: Dawid
 */

#ifndef APPLICATION_INTERRUPTSHANDLER_H_
#define APPLICATION_INTERRUPTSHANDLER_H_

#include "TimeManager.h"
#include "can.h"
#include "CanApp.h"
#include "CanNet.h"
#include "stm32l4xx_hal.h"
#include "IMU.h"
#include "stdbool.h"
#include "bluetooth.h"

extern volatile bool ImuFlag;  // flag used to block IMU data computing if communication with IMU module goes wrong ath the beginning

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);
//void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan);
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);

#endif /* APPLICATION_INTERRUPTSHANDLER_H_ */
