/*
 * bluetooth.c
 *
 *  Created on: 21.10.2021
 *      Author: Wiktor
 */

#include "bluetooth.h"
#include "usart.h"

BtTxFrameID ID[BtTxFrameID_Length] = {BATTERY_ENERGY_PARAMETERS_BT, BATTERY_OVERALL_PARAMATERS_BT, BATTERY_CHARGING_PARAMETERS_BT, MOTORCYCLE_OVERALL_PARAMETERS_BT, PERFORMANCE_PARAMETERS_BT};

uint8_t Tab_id = 0;
extern uint8_t Received[8];
extern uint8_t Rec_uart2;
extern uint8_t Rec_uart1;
Driving_Mode_En Driving_Mode = NORMAL;
uint8_t Auto_Lights;
uint8_t Home_Lights;
uint8_t Soc_Lights;
uint8_t Start_buzzer;
uint8_t Voice_control;
double Set_Latitude;
double Set_Longtitude;


void BtInit(void)
{
	//HAL_UART_Receive_IT(&huart2, Rec_uart2, 1);
	//HAL_UART_Receive_IT(&huart1, Received, 8);
}

void BtSendFrame(uint8_t *TxData, uint8_t size)
{
		HAL_UART_Transmit_IT(&huart1, TxData, size);
}

void BtSend(void)
{
	uint8_t frame[8];


	switch(ID[Tab_id])
	{
		case BATTERY_ENERGY_PARAMETERS_BT:
		{
			uint8_t Battery_SOC = 75;
			float Battery_Voltage = 10;
			uint8_t Battery_Voltage_LSB = ((uint16_t)(Battery_Voltage * 10)) & 0xFF;
			uint8_t Battery_Voltage_MSB = ((uint16_t)(Battery_Voltage * 10)) >> 8;
			uint8_t Battery_Temperature = 24;

			frame[0] = HEADER;
			frame[1] = (uint8_t)BATTERY_ENERGY_PARAMETERS_BT;
			frame[2] = 0;
			frame[3] = Battery_SOC;
			frame[4] = Battery_Voltage_LSB;
			frame[5] = Battery_Voltage_MSB;
			frame[6] = Battery_Temperature;
			frame[7] = 0;

			BtSendFrame(frame, 8);

		}
		break;


		case BATTERY_OVERALL_PARAMATERS_BT:
		{
			float Distance_Left = 50.56;
			uint8_t Distance_Left_LSB = ((uint16_t)(Distance_Left * 100)) & 0xFF;
			uint8_t Distance_Left_MSB = ((uint16_t)(Distance_Left * 100)) >> 8;
			uint16_t Charging_Cycles = 0x03;
			uint8_t Charging_Cycles_LSB = Charging_Cycles & 0xFF;
			uint8_t Charging_Cycles_MSB = Charging_Cycles >> 8;

			frame[0] = HEADER;
			frame[1] = (uint8_t)BATTERY_OVERALL_PARAMATERS_BT;
			frame[2] = 0;
			frame[3] = Distance_Left_LSB;
			frame[4] = Distance_Left_MSB;
			frame[5] = Charging_Cycles_LSB;
			frame[6] = Charging_Cycles_MSB;
			frame[7] = 0;

			BtSendFrame(frame, 8);


		}

		break;


		case BATTERY_CHARGING_PARAMETERS_BT:
		{
			bool Charging_Status = true;
			float Charging_Current = 32.53;
			uint8_t Charging_Current_LSB = ((uint16_t)(Charging_Current * 10)) & 0xFF;
			uint8_t Charging_Current_MSB = ((uint16_t)(Charging_Current * 10)) >> 8;
			uint8_t Time_To_Full_Charge = 12;

			frame[0] = HEADER;
			frame[1] = (uint8_t)BATTERY_CHARGING_PARAMETERS_BT;
			frame[2] = 0;
			frame[3] = Charging_Status;
			frame[4] = Charging_Current_LSB;
			frame[5] = Charging_Current_MSB;
			frame[6] = Time_To_Full_Charge;
			frame[7] = 0;


			BtSendFrame(frame, 8);


		}
		break;


		case MOTORCYCLE_OVERALL_PARAMETERS_BT:
		{
			float Milleage = 1023;
			uint8_t Milleage_LSB = ((uint16_t)(Milleage * 10)) & 0xFF;
			uint8_t Milleage_MSB = ((uint16_t)(Milleage * 10)) >> 8;
			float Odometer = 123.4;
			uint8_t Odometer_LSB = ((uint16_t)(Odometer * 100)) & 0xFF;
			uint8_t Odometer_MSB = ((uint16_t)(Odometer * 100)) >> 8;
			uint8_t Engine_Temp = 45;

			frame[0] = HEADER;
			frame[1] = (uint8_t)MOTORCYCLE_OVERALL_PARAMETERS_BT;
			frame[2] = 0;
			frame[3] = Engine_Temp;
			frame[4] = Milleage_LSB;
			frame[5] = Milleage_MSB;
			frame[6] = Odometer_LSB;
			frame[7] = Odometer_MSB;

			BtSendFrame(frame, 8);
			//HAL_UART_Transmit_IT(&huart2, "123", 3);


		}
		break;


		case PERFORMANCE_PARAMETERS_BT:
		{
			uint8_t Max_Speed = 20;
			uint8_t MAx_Angle = 14;
			uint8_t Max_Torque = 100;
			//uint8_t Max_Engine_Temp = 0x40;
			Driving_Mode_En Driving_Mode = SPORT;

			frame[0] = HEADER;
			frame[1] = (uint8_t)PERFORMANCE_PARAMETERS_BT;
			frame[2] = 0;
			frame[3] = (uint8_t)Driving_Mode;
			frame[4] = Max_Speed;
			frame[5] = MAx_Angle;
			frame[6] = Max_Torque;
			frame[7] = 0;



			BtSendFrame(frame, 8);

		}
		break;

		default:
			break;

	}


	Tab_id = (Tab_id < BtSendSize+1) ? Tab_id+1 : 1;

}

void BtReceive(void)
{
	HAL_UART_Transmit_IT(&huart2, "live", 4);
	// Filtracja poprawnosci ramek za pomoca ID

	if (Received[0] == 0x1E && Received[1] == ACTION_DATA)
	{
		switch(Received[2]) //zmiana trybu jazdy
		{
		case ECO:
		{
			Driving_Mode = ECO;
		}
		break;

		case NORMAL:
		{
			Driving_Mode = NORMAL;
		}
		break;

		case SPORT:
		{
			Driving_Mode = SPORT;
		}
		break;
		}

		Auto_Lights = Received[3];
		Home_Lights = Received[4];
		Soc_Lights = Received[5];
		Start_buzzer = Received[6];
		Voice_control = Received[7];

	} //end if
	else if (Received[0] == 0x69)  //ustawienie szerekosci geograficznej celu
	{
		uint64_t Received_Latitude = Received[0] | (uint64_t)(Received[1]) << 8 | (uint64_t)(Received[2]) << 16 | (uint64_t)(Received[3]) << 24 | (uint64_t)(Received[4]) << 32;
		Set_Latitude = Received_Latitude / (10e7);
		if (Received[7] == 1) Set_Latitude = -1 * Set_Latitude;
	} //end else if

	else if (Received[0] == 0x70) //ustawienie dlugosci geograficznej celu
	{
		uint64_t Received_Longtitude = Received[0] | (uint64_t)(Received[1]) << 8 | (uint64_t)(Received[2]) << 16 | (uint64_t)(Received[3]) << 24 | (uint64_t)(Received[4]) << 32;
		Set_Longtitude = Received_Longtitude / (10e7);
		if (Received[7] == 1) Set_Longtitude = -1 * Set_Longtitude;
	} //end else if
	else
	{
		for (uint8_t i=0; i<8; i++)
		{
			Received[i] = 0;
		}
	}

	uint8_t frame[8];
	frame[0] = 0xFF;
	frame[1] = 0xFF;
	frame[2] = 0xFF;
	frame[3] = 0xFF;
	frame[4] = 0xFF;
	frame[5] = 0xFF;
	frame[6] = 0xFF;
	frame[7] = 0xFF;

	HAL_UART_Transmit_IT(&huart2, "live", 4);

	HAL_UART_Receive_IT(&huart2, &Rec_uart2, 1);
	HAL_UART_Receive_IT(&huart1, &Rec_uart1, 1);
}
