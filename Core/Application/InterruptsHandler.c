/*
 * InterruptsHandler.c
 *
 *  Created on: Nov 24, 2020
 *      Author: Dawid
 */

#include "InterruptsHandler.h"

//_____________________________________________________

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);
//void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan);

//_____________________________________________________

/* DEBUG VARIABLES */
int debug=0;
volatile int new_debug = 0;
int time1=0;  // delay testing variables
int time2=0;
int imu_period=7;
int frame_counter = 0;


/* BLUETOOTH VARIABLES */
extern uint8_t Received[8]; //for uart received
extern uint8_t Rec_uart2;//for uart2 test
extern uint8_t Rec_uart1;//for uart1 test

/* TIME CONTROL STRUCTURE */
DelayTicks ActionsDelay={1,1}; // instance of struct uses for time control (should by filled only by "1")

/* CAN COMMUNICATION VARIABLES*/
CAN_RxHeaderTypeDef MyRxHeader;
volatile CanRxFrameID FrameID = NONE;
volatile CanMsg CanBusData = {0}; // can data instance
/* IMU FLAG */
volatile bool ImuFlag = true;

/* VELOCITY MEASUREMENT VARIABLES */

volatile uint16_t ImpulseCounter = 0;
volatile uint16_t debounce = 0;
extern uint16_t TicCounter;

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){


	 if(htim->Instance == TIM7)
		 {
		 	 debug++;
		 	 TimeControl(&ActionsDelay);
		 }

	 if(htim->Instance == TIM6)
		 {
		 	 new_debug++;
		 	 time1 = HAL_GetTick();

		 	 if(ImuFlag) // only if communication with IMU module is OK
		 	 {
		 		 IMU_Loop();
		 	 }

		 	 CanSendDashboardData(); // cycling sending data to the dashboard
		 	 time2 = HAL_GetTick();
		 	 imu_period = time2-time1;
		 }

}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

// gowno do sprawdzenia czy dzialaja przerwania
	if(huart->Instance == USART1)
	{
		HAL_UART_Transmit_IT(&huart2, "1234567980", 10);
		HAL_UART_Receive_IT(&huart1, &Rec_uart1, 1);

	 }
	if(huart->Instance == USART2)
	{
		HAL_UART_Transmit_IT(&huart2, "przerwanie", 10);
		HAL_UART_Receive_IT(&huart2, &Rec_uart2, 1);

	}
} //end of UART callback function


void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	//time1=HAL_GetTick();

	//while(HAL_CAN_GetRxMessage(&hcan1, CAN_RX_FIFO0, &MyRxHeader, CanBusData.RxData)!=HAL_OK){};
	if(HAL_CAN_GetRxMessage(&hcan1, CAN_RX_FIFO0, &MyRxHeader, CanBusData.RxData)!=HAL_OK)
	{
		Error_Handler();
	}

	FrameID = MyRxHeader.ExtId;
	//CanReceiveData(FrameID, &CanBusData);
	frame_counter++;
	//time2=HAL_GetTick();
	//delta_t=time2-time1;

}//end of CAN callback function

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == VEL_MEASURE_Pin)
	{
		if(TicCounter - debounce > 10)
		{
			ImpulseCounter++;
			debounce = TicCounter;
		}
	}
}







