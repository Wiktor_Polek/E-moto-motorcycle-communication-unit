/*
 * AsynchActionsManager.c
 *
 *  Created on: Nov 24, 2020
 *      Author: Dawid
 */


#include "AsynchActionsManager.h"
#include "speedsensor.h"

ActionsFlags Flags = {0};

void AsynchActionsHandler(void)
{

	if(Flags.SpeedSensorFlag)
	{
		speed_measurement();
		Flags.SpeedSensorFlag = false;
		uart_send(_INFO_, "Gateway Loop !!!", 1.0, 1.0);
	}

	if(Flags.BluetoothFlag)
	{
		BtSend();
		Flags.BluetoothFlag = false;
	}

}
