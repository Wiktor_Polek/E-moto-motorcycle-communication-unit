/*
 * AsynchActionsManager.h
 *
 *  Created on: Nov 24, 2020
 *      Author: Dawid
 */

#ifndef APPLICATION_ASYNCHACTIONSMANAGER_H_
#define APPLICATION_ASYNCHACTIONSMANAGER_H_

#include <stdbool.h>
#include "terminal.h"

void AsynchActionsHandler(void);

typedef struct{				// struct to store flags for triggering asynchronous actions

	volatile bool SpeedSensorFlag;
	volatile bool BluetoothFlag;
}ActionsFlags;


#endif /* APPLICATION_ASYNCHACTIONSMANAGER_H_ */
