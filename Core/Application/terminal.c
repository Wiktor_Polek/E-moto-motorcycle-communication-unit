/*
 * terminal.c
 *
 *  Created on: 5 maj 2021
 *      Author: Adam Witczak
 */

#include <string.h>
#include <stdio.h>

#include "terminal.h"

//_____________________________________________________

void uart_send(MSG_TYPE msg, char info[], double value1, double value2);

//_____________________________________________________

void uart_send(MSG_TYPE msg, char info[], double value1, double value2)
{
	char value1Buffer[64], value2Buffer[64];
	char data[150] = "\r";
	char *msg_type[] = {"_OK_  ","_ERR_ ","_INFO_"};
	char *temp_msg_type;

	sprintf(value1Buffer,"%lf", value1);
	snprintf(value2Buffer, sizeof value2Buffer, "%lf", value2);

	switch(msg)
	{
		case _OK_:
			temp_msg_type = msg_type[_OK_];
			break;
		case _ERR_:
			temp_msg_type = msg_type[_ERR_];
			break;
		case _INFO_:
			temp_msg_type = msg_type[_INFO_];
			break;
	}

	// generating a message string
	strcat(data, temp_msg_type);
	strcat(data," | ");
	strcat(data, info);
	strcat(data," | ");
	strcat(data, "Val_1: ");
	strcat(data, value1Buffer);
	strcat(data," | ");
	strcat(data, "Val_2: ");
	strcat(data, value2Buffer);
	strcat(data,"\n\r");

	HAL_UART_Transmit_IT(&huart2, data, strlen(data));
}
