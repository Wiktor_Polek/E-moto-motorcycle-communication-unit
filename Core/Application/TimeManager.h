/*
 * TimeManager.h
 *
 *  Created on: 13 lis 2020
 *      Author: Dawid
 */

#ifndef INC_TIMEMANAGER_H_
#define INC_TIMEMANAGER_H_

#include <stdint.h>
#include "stm32l4xx_hal.h"

#include "CanApp.h"
#include "CanNet.h"
#include "AsynchActionsManager.h"
//#include "bluetooth.h"

/* definitions of os delays for each action in tick unit */

#define			BT_DELAY				40 // example value 40*20[ms]=800[ms]
#define			SPEED_SENSOR_DELAY		5

typedef struct
{
	volatile int BtSendTick;
	volatile int SpeedSensorTick;

}DelayTicks;

void TimeManagerInit(TIM_HandleTypeDef *ChosenTimer);
void TimeControl(DelayTicks* Ticks);
void IWDG_Refresh(IWDG_HandleTypeDef *Watchdog);

#endif /* INC_TIMEMANAGER_H_ */
