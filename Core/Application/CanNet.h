/*
 * CanNet.h
 *
 *  Created on: 19 gru 2020
 *      Author: Dawid
 */

#ifndef APPLICATION_CANNET_H_
#define APPLICATION_CANNET_H_

#include "CanApp.h"
#include "DataTypes.h"
#include "IMU.h"
#include <stdbool.h>
#include <stdlib.h>

#define BMS_BASE_ADDR 6581 //91

typedef enum
{
	NONE,

	/* FRAMES RECEIVED FROM BMS */

	BAT_VOLTAGE = 0x01,
	CELL_TEMP = 0x02,
	BAT_CHARGE = 0x0500,
	BAT_DISTANCE = 0x0600,
	BMS_STATS = 0x0404,
	BMS_EVENTS = 0x0405
    //OVERALL_PARAM,
	//BAT_ENERGY_PARAM,
	//BAT_OVERALL_PARAM,
	//MOTOR_OVERALL_PARAM,
	//PERFORMANCE_PARAM,
	//ENGINE_PARAM,
	//LIGHTS_PARAM,
	//LAT_MOTO,
	//LON_MOTO,
	//LAT_DESTINATION,
	//LON_DESTINATION,
	//IMU_PARAM,
	//SETTINGS_PARAM,
	//INPUT_PARAM

}CanRxFrameID;

typedef enum
{
	OVERALL_PARAM,
	LIGHTS_PARAM,
	LON_MOTO,
	LAT_DESTINATION,
	LON_DESTINATION,
	IMU_PARAM,
	SETTINGS_PARAM,
	INPUT_PARAM

}CanTxFrameID;

typedef enum
{
	NONE_STAT,
	CHRG_CYCLES = 35,
	CHRG_TIME = 5  // TOTAL CHRG TIME ???????????????????
}
BMS_stats;

typedef enum
{
	OVERALL_PARAMETERS = 0x12B50001,
	BATTERY_ENERGY_PARAMETERS = 0x12B50002,
	BATTERY_OVERALL_PARAMETERS = 0x12B50003,
	MOTORCYCLE_OVERALL_PARAMETERS = 0x12B50004,
	PERFORMANCE_PARAMETERS = 0x12B50005,
	DEST_LAT_PARAMETERS = 0x12B5000A,
	DEST_LON_PARAMETERS = 0x12B5000B,
	IMU_PARAMETERS = 0x12B5000C,
	DSB_MAX_ENUM = 0x08

}DashBrdFramesID_en;

typedef struct
{
	double dblVal;
	uint8_t bytes[5];
	uint8_t sign;
	uint8_t decSign;
}gps_val;

void CanReceiveData(volatile CanRxFrameID FrameId, volatile CanMsg * CanData);
void CanSendDashboardData(void);

#endif /* APPLICATION_CANNET_H_ */
