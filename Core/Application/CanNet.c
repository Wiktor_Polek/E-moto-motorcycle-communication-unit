/*
 * CanNet.c
 *
 *  Created on: 19 gru 2020
 *      Author: Dawid
 */

#include "CanNet.h"

/* DATA TO SEND VIA CAN-bus */
extern volatile IMU_data_t IMU_Data;
/* ________________________ */

volatile NetData NetworkData = {0};
BMS_stats Stat = NONE_STAT;
volatile DashBrdFramesID_en ID_table[DSB_MAX_ENUM] = {OVERALL_PARAMETERS, BATTERY_ENERGY_PARAMETERS, BATTERY_OVERALL_PARAMETERS,MOTORCYCLE_OVERALL_PARAMETERS,PERFORMANCE_PARAMETERS,DEST_LAT_PARAMETERS,DEST_LON_PARAMETERS,IMU_PARAMETERS};
volatile uint8_t Tab_ind = 0;
volatile gps_val coordinate = {0};

static void coord_to_bytes(volatile gps_val *coords)
{
	long long int moto_lat_temp = abs( ((int)(coords->dblVal * 10000000)));
    coords->bytes[4] = (uint8_t)(moto_lat_temp >> 32);
    coords->bytes[3] = (uint8_t)(moto_lat_temp>>24);
    coords->bytes[2] = (uint8_t)(moto_lat_temp>>16);
    coords->bytes[1] = (uint8_t)(moto_lat_temp>>8);
    coords->bytes[0] = (uint8_t)(moto_lat_temp&0xFF);

    if((coords->dblVal) >= 0) coords->sign = 0;
    else coords->sign = 1;

    if(abs((int)(coords->dblVal)) > 0 && abs((int)(coords->dblVal)) < 10)	 		coords->decSign = 8;
    else if(abs((int)(coords->dblVal)) >= 10 && abs((int)(coords->dblVal)) < 100) 	coords->decSign = 7;
    else if(abs((int)(coords->dblVal)) >= 100 && abs((int)(coords->dblVal)) <= 180)	coords->decSign = 6;
}

void CanReceiveData(volatile CanRxFrameID FrameId, volatile CanMsg * CanData)
{

	switch(FrameId)
		{
			case BAT_VOLTAGE:
			{
				NetworkData.bms.voltage_cellavg = CanData->RxData[2]*0.01+2.0;  // [V]
				NetworkData.bms.voltage_total = (CanData->RxData[4]+(CanData->RxData[3] << 8)+(CanData->RxData[6] << 16) + (CanData->RxData[5] << 24))*0.01;
			}
			break;


			case CELL_TEMP:
			{
				NetworkData.bms.avg_celltemp = CanData->RxData[2] - 100;    // in Celsius Degrees
				NetworkData.bms.max_celltemp = CanData->RxData[1] - 100;
			}
			break;

			case BAT_CHARGE:
			{
				/* Move MSB 8 bits -> add LSB -> cast at signed 16bit integer */
				NetworkData.bms.current = ((int16_t)((CanData->RxData[0]<<8) + CanData->RxData[1]))*0.1; // [A]
				NetworkData.bms.charge_left = CanData->RxData[6];	// [%]
			}
			break;

			case BAT_DISTANCE:
			{
				NetworkData.bms.distance_left = ((CanData->RxData[4]<<8) + CanData->RxData[5])*0.1; // [chosen distance unit -- I assumed km]
				NetworkData.bms.distance_total = ((CanData->RxData[6]<<8) + CanData->RxData[7])*0.1;
			}
			break;

			case BMS_STATS:
			{
				switch(CanData->RxData[1]) // switch depends on BMS statictic identyfier
				{

					case CHRG_CYCLES:
					{
							NetworkData.bms.cycles = CanData->RxData[5] + (CanData ->RxData[4] << 8);
					}
					break;

					case CHRG_TIME:
					{
							NetworkData.bms.charge_time = CanData->RxData[5]+(CanData->RxData[4] << 8)+(CanData->RxData[3] << 16) + (CanData->RxData[2] << 24);
					}
					break;
				}

			}
			break;

			case BMS_EVENTS:
			{
				if(CanData->RxData[1] == 0)  // only if it is event info data frame
				{
					switch(CanData->RxData[2]) // deal with data depending on event identifier
					{
						case 0:  // if there is no alarm occured
						{
							NetworkData.bms.event.chrg = NO_CHRG;
							NetworkData.bms.event.high_current = false;
							NetworkData.bms.event.low_voltage = false;
							NetworkData.bms.event.temp_alarm = false;
							NetworkData.bms.event.low_voltage_alarm = false;
							NetworkData.bms.event.high_voltage_alarm = false;
						}
						break;

						case 4: /* CELLS VOLTAGE CRITICALLY LOW */
						{
							NetworkData.bms.event.low_voltage_alarm = true;  // voltage alarm occured
						}
						break;

						case 6: /* CELLS VOLTAGE CRITICALLY HIGH */
						{
							NetworkData.bms.event.high_voltage_alarm = true; // voltage alarm occured
						}
						break;

						case 8: /* DISCHARGE CURRENT CRITICALLY HIGH */
						{
							NetworkData.bms.event.high_current = true;
						}
						break;

						case 16: /* WARNING - LOW VOLTAGE - REDUCING POWER */
						{
							NetworkData.bms.event.low_voltage = true;
						}
						break;

						case 42: /* CELL TEMPERATURE CRITICALLY HIGH */
						{
							NetworkData.bms.event.temp_alarm = true;
						}
						break;

						case 25: /* STARTED PRECHARGING STAGE */
						{
							NetworkData.bms.event.chrg = PRE_CH;
						}
						break;

						case 26: /* STARTED MAIN CHARGING STAGE */
						{
							NetworkData.bms.event.chrg = MAIN_CH;
						}
						break;

						case 27: /* STARTED BAlANCING STAGE */
						{
							NetworkData.bms.event.chrg = BALANCING;
						}
							break;

						case 28: /* CHARGING FINISHED */
						{
							NetworkData.bms.event.chrg = FINISHED;
						}
						break;
					}

				}

			}
			break;

			default:
			{
				// do nothing
			}
			break;


		}

}

void CanSendDashboardData(void)
{

	uint8_t frame[8];

	switch(ID_table[Tab_ind])
	{
		case OVERALL_PARAMETERS:
		{
			float velocity = 7;
			bool bt_state = true;
			uint8_t travel_time = 50;
			uint8_t distance = 13;
			uint8_t distance_left = 10;
			uint8_t last_recharge_day = 4;
			uint8_t last_recharge_month = 3;
			uint8_t last_recharge_year = 21;

			frame[0] = (uint8_t)velocity;
			frame[1] = (uint8_t)bt_state;
			frame[2] = travel_time;
			frame[3] = distance;
			frame[4] = distance_left;
			frame[5] = last_recharge_day;
			frame[6] = last_recharge_month;
			frame[7] = last_recharge_year;

			CanSendFrame((uint32_t)OVERALL_PARAMETERS, frame, 8);
		}
		break;

		case BATTERY_ENERGY_PARAMETERS:
		{
			uint8_t bat_level = 77;
			float battery_voltage = 97.984;
			float bat_temp = 34.232;
			float bat_voltage_MAX = 100.321;
			float bat_voltage_MIN = 89.223;

			frame[0] = bat_level;
			frame[1] = ((uint16_t)((battery_voltage)*10)) & 0xFF;
			frame[2] = (((uint16_t)((battery_voltage)*10)) >> 8);
			frame[3] = bat_temp;
			frame[4] = ((uint16_t)((bat_voltage_MAX)*10)) & 0xFF;
			frame[5] = (((uint16_t)((bat_voltage_MAX)*10)) >> 8);
			frame[6] = ((uint16_t)((bat_voltage_MIN)*10)) & 0xFF;
			frame[7] = (((uint16_t)((bat_voltage_MIN)*10)) >> 8);

			CanSendFrame((uint32_t)BATTERY_ENERGY_PARAMETERS, frame, 8);
		}
		break;

		case BATTERY_OVERALL_PARAMETERS:
		{
			bool charging_status = false;
			float charging_current = 3.345;
			float time_to_full_charge = 1.234;
			uint16_t charging_cycles = 123;

			frame[0] = charging_status;
			frame[1] = ((uint16_t)((charging_current)*10)) & 0xFF;
			frame[2] = (((uint16_t)((charging_current)*10)) >> 8);
			frame[3] = ((uint16_t)((time_to_full_charge)*10)) & 0xFF;
			frame[4] = (((uint16_t)((time_to_full_charge)*10)) >> 8);
			frame[5] = charging_cycles & 0xFF;
			frame[6] = charging_cycles >> 8;
			frame[7] = 0;

			CanSendFrame((uint32_t)BATTERY_OVERALL_PARAMETERS, frame, 8);
		}
		break;

		case MOTORCYCLE_OVERALL_PARAMETERS:
		{
			float milleage = 23.543;
			float odometer = 123.435;
			uint8_t max_power = 33;
			uint8_t best_recharge = 77;
			uint8_t max_runtime = 5;

			frame[0] = ((uint16_t)((milleage)*10)) & 0xFF;
			frame[1] = (((uint16_t)((milleage)*10)) >> 8);
			frame[2] = ((uint16_t)((odometer)*10)) & 0xFF;
			frame[3] = (((uint16_t)((odometer)*10)) >> 8);
			frame[4] = max_power;
			frame[5] = best_recharge;
			frame[6] = max_runtime;
			frame[7] = 0;

			CanSendFrame((uint32_t)MOTORCYCLE_OVERALL_PARAMETERS, frame, 8);
		}
		break;

		case PERFORMANCE_PARAMETERS:
		{
			float max_velocity = 87.574;
			float max_angle = 33.234;
			float max_torque = 45.32;
			float max_engine_temp = 100.457;

			frame[0] = max_velocity;
			frame[1] = max_angle;
			frame[2] = max_torque;
			frame[3] = max_engine_temp;
			frame[4] = 0;
			frame[5] = 0;
			frame[6] = 0;
			frame[7] = 0;

			CanSendFrame((uint32_t)PERFORMANCE_PARAMETERS, frame, 8);
		}
		break;

		case DEST_LAT_PARAMETERS:
		{
			coordinate.dblVal = -12.543324;
			coord_to_bytes(&coordinate);

			for(uint8_t i = 0; i < 5; i++)
			{
				frame[i] = coordinate.bytes[i];
			}

			frame[5] = coordinate.decSign;
			frame[6] = coordinate.sign;
			frame[7] = 0;

			CanSendFrame((uint32_t)DEST_LAT_PARAMETERS, frame, 8);
		}
		break;

		case DEST_LON_PARAMETERS:
		{
			coordinate.dblVal = 33.543324;
			coord_to_bytes(&coordinate);

			for(uint8_t i = 0; i < 5; i++)
			{
				frame[i] = coordinate.bytes[i];
			}

			frame[5] = coordinate.decSign;
			frame[6] = coordinate.sign;
			frame[7] = 0;

			CanSendFrame((uint32_t)DEST_LON_PARAMETERS, frame, 8);
		}
		break;

		case IMU_PARAMETERS:
		{
			float roll = IMU_Data.roll;
			float pitch = IMU_Data.pitch;
			float a_x = IMU_Data.a_x;
			float a_y = IMU_Data.a_y;
			float a_z = IMU_Data.a_z;

			frame[0] = (int8_t)roll;
			frame[1] = (int8_t)pitch;
			frame[2] = ((int16_t)((a_x)*1000)) & 0xFF;
			frame[3] = (((int16_t)((a_x)*1000)) >> 8);
			frame[4] = ((int16_t)((a_y)*1000)) & 0xFF;
			frame[5] = (((int16_t)((a_y)*1000)) >> 8);
			frame[6] = ((int16_t)((a_z)*1000)) & 0xFF;
			frame[7] = (((int16_t)((a_z)*1000)) >> 8);

			CanSendFrame((uint32_t)IMU_PARAMETERS, frame, 8);
		}
		break;

		default:
			break;
	}// end switch

	Tab_ind++;

	if(!(Tab_ind < (uint8_t)DSB_MAX_ENUM))
		Tab_ind = 0;
}
