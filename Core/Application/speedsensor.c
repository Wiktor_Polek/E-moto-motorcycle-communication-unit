/*
 * speedsensor.c
 *
 *  Created on: Jul 9, 2020
 *      Author: mikolaj
 */
#include "speedsensor.h"
#include "stm32l4xx_hal.h"

volatile uint16_t	last_pps; //ostatnia ilosc impulsów na sekunde
int  speed =0;

int speed_measurement(void)
{

	speed=(last_pps*WHEEL_CIRCUIT_MM*36)/(MAGNES_NUMBER*10000);//prędkosc w km/h

	return speed;
}
