
#include "TimeManager.h"


int debug_2=0;
extern ActionsFlags Flags;

//______________________________________________
void TimeManagerInit(TIM_HandleTypeDef * ChosenTimer);
void TimeControl(DelayTicks* Ticks);
static void IncreaseTicks(DelayTicks* Ticks);
void IWDG_Refresh(IWDG_HandleTypeDef *Watchdog);
//______________________________________________

void TimeManagerInit(TIM_HandleTypeDef * ChosenTimer)
{
	HAL_TIM_Base_Start_IT(ChosenTimer);
}

static void IncreaseTicks(DelayTicks* Ticks)
{
	/* Increase each tick */
	Ticks->BtSendTick++;
	Ticks->SpeedSensorTick++;
}

void TimeControl(DelayTicks* Ticks)
{
	/* Functions increasing and control current ticks for each periodical action */
	debug_2++;

	if(Ticks->BtSendTick >= BT_DELAY)
	{
		Flags.BluetoothFlag = true;
		Ticks->BtSendTick = 0; // reset tick for an action

	}

	if(Ticks->SpeedSensorTick >= SPEED_SENSOR_DELAY)
	{
		Flags.SpeedSensorFlag = true;
		Ticks->SpeedSensorTick = 0;
	}

	/* Increase each tick */
	IncreaseTicks(Ticks);

}


void IWDG_Refresh(IWDG_HandleTypeDef *Watchdog)
{
	HAL_IWDG_Refresh(Watchdog); // reload watchdog timer register
}
