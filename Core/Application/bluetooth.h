#include "stdio.h"
#include "main.h"

#include "stm32l4xx_hal_uart.h"
#include "stdbool.h"
#include "stdlib.h"

#define BtTxFrameID_Length 5
#define BtSendSize 5
#define HEADER 0X1E


typedef enum
{
	BATTERY_ENERGY_PARAMETERS_BT = 	0X1,
	BATTERY_OVERALL_PARAMATERS_BT = 	0X2,
	BATTERY_CHARGING_PARAMETERS_BT = 	0X3,
	MOTORCYCLE_OVERALL_PARAMETERS_BT = 0X4,
	PERFORMANCE_PARAMETERS_BT =		0X5,

}BtTxFrameID;

typedef enum
{
	ACTION_DATA =				0X6,
	GPS_LATITUDE = 				0x69,
	GPS_LONGTITUDE = 			0x70,
}BtRxFrameID;

typedef enum
{
	ECO = 0,
	NORMAL = 1,
	SPORT = 2
}Driving_Mode_En;

void BtInit(void);

//Function which sends data frames via UART
void BtSendFrame(uint8_t *TxData, uint8_t size);
//Function which prepares data frames to be sent
void BtSend(void);
//Function which prepares receiving data
void BtReceive(void);




