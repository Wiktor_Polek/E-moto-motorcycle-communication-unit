/*
 * speedsensor.h
 *
 *  Created on: Jul 9, 2020
 *      Author: mikolaj
 */


#ifndef APPLICATION_SPEEDSENSOR_H_
#define APPLICATION_SPEEDSENSOR_H_

#define WHEEL_CIRCUIT_MM 2000 //obw koła w mm
#define MAGNES_NUMBER 2			//ilośc magnesów na kole


int speed_measurement();


#endif /* APPLICATION_SPEEDSENSOR_H_ */
